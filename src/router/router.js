import Vue from 'vue'
import store from '../store/store'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Container from "../views/Container.vue"
import Proposal from "../views/Proposal.vue"
import Admin from "../views/Admin.vue"
import History from "@/views/History";
import Ranking from "@/views/Ranking";
import { Notify } from 'quasar'

Vue.use(VueRouter)

const router = new VueRouter({ routes: [
 
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    component: Container,
    meta: { requiresAuth: true },
    children: [
      {
        path: 'home',
        name: 'home',
        component: Home
      },
      {
        path: "proposal",
        name: "proposal",
        component: Proposal
      },
      {
        path: "admin",
        name: "admin",
        component: Admin,
        beforeEnter(to, from, next) {
          if (!store.state.user.is_superuser) {
            Notify.create({
              message: `<span style="font-family:Yekan;float:right;font-size:15px;">شما اجازه دسترسی به این بخش را ندارید</span>`,
              color: "negative",
              html: true
            });
            next('/home');
          } else {
            next();
          }
        }
      },
      {
        path: "history",
        name: "history",
        component: History
      },
      {
        path: "ranking",
        name: "ranking",
        component: Ranking
      }
    ]
  },
  {
    path: '*',
    name: 'notFound',
    redirect: { name: "home" }
  }
  

]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters['isLoggedIn']) {
      next();
    } else {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      });
    }
  } else {
    next();
  }
});

export default router

import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
// import { componnet} from "../helpers"

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    phoneNum: localStorage.getItem("phoneNum") || "",
    accessToken: localStorage.getItem("accessToken") || "",
    refreshToken: localStorage.getItem("refreshToken") || "",
    apiKey: "39954521-7280-49d5-9d65-6e6ed0110e87",
    siteKey:"6LcKH3UbAAAAAMo0ZpoHb6am9I8-pMhFFMCkaZzH",
    currentUser: {},
    mobile: false,
    scores: [],
    students: [],
    teachers: [],
    industries: [],
    admin: false,
    channel: [],
    video: {},
    proposal: {},
    proposals:[],
    user: {},
    history:[],
    uploadTime: false,
    scoreTime: false,
    
  },
  getters: {
    isLoggedIn: state => !!state.accessToken,
    getProposal: state => {
      return state.proposal
    }
  },
  mutations: {
    REPLACE_TOKEN(state, tokens) {
      localStorage.setItem("accessToken", tokens.access);
      state.accessToken = tokens.access;
    },
    SET_USER_TYPE(state, payload) {
      state.user = payload;
    },
    SET_USER(state, phoneNUM) {
      state.currentUser["phoneNum"] = phoneNUM;
    },
    LOGIN(state, token) {
      state.accessToken = token.access;
      state.refreshToken = token.refresh;
    },
    LOGOUT(state) {
      state.accessToken = "";
      state.refreshToken = "";
    },
    SET_CHANNEL(state, payload) {
      state.channel = payload;
    },
    SET_PROPOSAL(state, payload) {
      state.proposal = payload;
    },
    SET_PROPOSALS(state,payload){
        state.proposals = payload
    },
    SET_STUDENTS(state, payload) {
      state.students = payload;
    },
    SET_TEACHERS(state, payload) {
      state.teachers = payload;
    },
    SET_INDUSTRIES(state, payload) {
      state.industries = payload;
    },
    UPDATE_STUDENT(state, payload) {
      state.students = state.students.map(user =>
        user.id === payload.id ? payload : user
      );
    },
    UPDATE_TEACHER(state, payload) {
      state.teachers = state.teachers.map(user =>
        user.id === payload.id ? payload : user
      );
    },
    UPDATE_GRADE(state, payload) {
      state.proposals = state.proposals.map(user =>{
        if (user.id === payload.proposal_id ){
          user.grade = payload.student_grade;
          return user
        }else{
          return user
        }


          }
      );
    },
    ADD_STUDENT(state, payload) {
      state.students.push(payload);
    },
    DELETE_STUDENT(state, id) {
      let index = state.students.findIndex(student => student.id == id)
      state.students.splice(index, 1)
    },
    DELETE_TEACHER(state, id) {
      let index = state.teachers.findIndex(teacher => teacher.id == id)
      state.teachers.splice(index, 1)
    },
    DELETE_INDUSTRY(state, id) {
      let index = state.industries.findIndex(industry => industry.id == id)
      state.industries.splice(index, 1)
    },
    ADD_TEACHER(state, payload) {
      state.teachers.push(payload);
    },
    ADD_INDUSTRY(state, payload) {
      state.industries.push(payload);
    },
    DELETE_PROPOSAL(state) {
      state.proposal = {};
      state.proposal = { ...state.proposal };
    },
    SET_RANKNING(state ,payload) {
      state.scores = payload
    },
    SET_HISTORY(state,payload){
      state.history = payload;
    },
    SET_PERMISSION(state, payload) {
      let now = new Date(); //current Date that gives us current Time also

      let startTime = payload.proposal_submit_start;
      let endTime = payload.proposal_submit_due;
      let secstartTime = payload.grade_submit;
      let secendTime = payload.grade_due;
      var s = startTime.split(":");
      var dt1 = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        parseInt(s[0]),
        parseInt(s[1]),
        parseInt(s[2])
      );

      var e = endTime.split(":");
      var dt2 = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        parseInt(e[0]),
        parseInt(e[1]),
        parseInt(e[2])
      );

      state.uploadTime = now >= dt1 && now <= dt2;
      var s2 = secstartTime.split(":");
      var dt12 = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        parseInt(s2[0]),
        parseInt(s2[1]),
        parseInt(s2[2])
      );

      var e2 = secendTime.split(":");
      var dt22 = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        parseInt(e2[0]),
        parseInt(e2[1]),
        parseInt(e2[2])
      );
      state.scoreTime = now >= dt12 && now <= dt22;
    }
  },
  actions: {
    getHistory({commit,state}){
      return new Promise((resolve, reject) => {
        axios
          .get("/get/grades", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_HISTORY", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getRankning({commit,state}){
      return new Promise((resolve, reject) => {
        axios
          .get("/proposal/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_RANKNING", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    postScore({commit,state},payload){
      return new Promise((resolve, reject) => {
        axios
          .post("/proposal/"+payload.id+`/${state.user.is_staff && !state.user.is_superuser && !state.user.is_industry ?  'instructor_grade' :'student_grade'}/`, payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("UPDATE_GRADE", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });

    },
    putScore({commit,state},payload){
      return new Promise((resolve, reject) => {
        axios
          .put("/proposal/"+payload.id+`/${state.user.is_staff && !state.user.is_superuser && !state.user.is_industry ?  'instructor' :'student_grade/'}`, payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("UPDATE_GRADE", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });

    },
    getTimeSlot({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/admin/semester/get_current_semester/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_PERMISSION", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getUser({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/get/user", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_USER_TYPE", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    postStudent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/admin/students/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("ADD_STUDENT", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    deleteStudent({ commit, state }, id) {
      return new Promise((resolve, reject) => {
        axios
            .delete("/admin/students/" + id, {
              headers: {
                Authorization: `Bearer ${state.accessToken}`
              }
            })
            .then(response => {
              commit("DELETE_STUDENT", id);
              resolve(response.data);
            })
            .catch(error => {
              reject(error);
            });
      });
    },
    deleteTeacher({ commit, state }, id) {
      return new Promise((resolve, reject) => {
        axios
            .delete("/admin/instructors/" + id, {
              headers: {
                Authorization: `Bearer ${state.accessToken}`
              }
            })
            .then(response => {
              commit("DELETE_TEACHER", id);
              resolve(response.data);
            })
            .catch(error => {
              reject(error);
            });
      });
    },
    deleteIndustry({ commit, state }, id) {
      return new Promise((resolve, reject) => {
        axios
            .delete("/admin/industries/" + id, {
              headers: {
                Authorization: `Bearer ${state.accessToken}`
              }
            })
            .then(response => {
              commit("DELETE_INDUSTRY", id);
              resolve(response.data);
            })
            .catch(error => {
              reject(error);
            });
      });
    },
    postTeacher({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/admin/instructors/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("ADD_TEACHER", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    postIndustry({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/admin/industries/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("ADD_INDUSTRY", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    requestSignUpIn(store,payload) {
      return new Promise((resolve, reject) => {
        axios
            .post("/admin/industries/sign_up/", payload, {
            })
            .then(response => {
              resolve(response.data);
            })
            .catch(error => {
              reject(error);
            });
      });
    },

    putStudent({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .put("/admin/students/" + payload.id + "/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("UPDATE_STUDENT", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    putTeacher({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .put("/admin/instructors/" + payload.id + "/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("UPDATE_TEACHER", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    putIndustry({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .put("/admin/industries/" + payload.id + "/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("UPDATE_INDUSTRY", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getTeacher({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/admin/instructors/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_TEACHERS", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getIndustry({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/admin/industries/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_INDUSTRIES", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getStudents({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/admin/students/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_STUDENTS", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getProposals({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/proposal/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_PROPOSALS", response.data);
            resolve(response.data[0]);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getProposal({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("/proposal/get_proposal", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_PROPOSAL", response.data);
            resolve(response.data);
          })
          .catch(error => {
            commit("SET_PROPOSAL", {});
            reject(error);
          });
      });
    },
    uploadProposal({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/proposal/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_PROPOSAL", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    editProposal({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .put("/proposal/" + state.proposal.id + "/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("SET_PROPOSAL", response.data);
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    deleteProposal({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .delete("/proposal/" + state.proposal.id + "/", {
            headers: {
              Authorization: `Bearer ${state.accessToken}`
            }
          })
          .then(response => {
            commit("DELETE_PROPOSAL");
            resolve(response.data);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getChannel({ commit, state }) {
      return new Promise((resolve, reject) => {
        axios
          .get("https://napi.arvancloud.com/vod/2.0/channels", {
            headers: {
              Authorization: `Apikey ${state.apiKey}`
            }
          })
          .then(response => {
            commit("SET_CHANNEL", response.data.data[0]);
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    confirmVideo({ state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post(
            `https://napi.arvancloud.com/vod/2.0/channels/${state.channel.id}/videos`,
            payload,
            {
              headers: {
                Authorization: `Apikey ${state.apiKey}`
              }
            }
          )
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getNewToken({ commit, dispatch, state }) {
      return new Promise((resolve, reject) => {
        axios
          .post("/token/refresh/", { refresh: state.refreshToken })
          .then(response => {
            commit("REPLACE_TOKEN", response.data);
            resolve(response);
          })
          .catch(error => {
            dispatch("logoutNoRequest");
            reject(error);
          });
      });
    },
    login({ commit, dispatch }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/token/", payload)
          .then(response => {
            const tokens = response.data;
            commit("LOGIN", tokens);
            localStorage.setItem("accessToken", tokens.access);
            localStorage.setItem("refreshToken", tokens.refresh);
            axios.defaults.headers.common[
              "Authorization"
            ] = `Bearer ${tokens.access}`;
            resolve(response);
          })
          .catch(error => {
            dispatch("logoutNoRequest");
            reject(error);
          });
      });
    },
    logout({ commit }) {
      // return new Promise((resolve, reject) => {
      //   axios
      //     .post("http://192.168.43.177:8080/api/token/logout/", payload)
      //     .then(response => {
      //       commit("LOGOUT");
      //       localStorage.removeItem("accessToken");
      //       delete axios.defaults.headers.common["Authorization"];
      //       resolve(response);
      //     })
      //     .catch(reject);
      // });
      commit("LOGOUT");
      localStorage.removeItem("accessToken");
      delete axios.defaults.headers.common["Authorization"];
    },
    logoutNoRequest({ commit }) {
      return new Promise(resolve => {
        commit("LOGOUT");
        localStorage.removeItem("accessToken");
        delete axios.defaults.headers.common["Authorization"];
        resolve();
      });
    },
    sendCode({ commit }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/send-code/", payload)
          .then(response => {
            resolve(response);
            localStorage.setItem("phone_num", payload["phone_num"]);
            commit("SET_USER", payload["phone_num"]);
          })
          .catch(reject);
      });
    },
    uploadDoc({ state }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post("/proposal/", payload, {
            headers: {
              Authorization: `Bearer ${state.accessToken}`,
              "Content-Type": "multipart/form-data"
            }
          })
          .then(response => {
            resolve(response);
          })
          .catch(reject);
      });
    }
  }
});

import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import router from './router/router'
import store from './store/store'
import './quasar'
import "./assets/fonts/fonts.css"
import './quasar'
import Recaptcha from "vue-recaptcha"
Vue.use(Recaptcha)
Vue.prototype.$http = Axios;
Vue.prototype.$http.defaults.baseURL = process.env.VUE_APP_ROOT_API;

const accessToken = localStorage.getItem('accessToken');
if (accessToken) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
}

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

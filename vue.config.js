module.exports = {
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: true,
      rtl:true
    }
  },
  transpileDependencies: [
    'quasar'
  ],
  chainWebpack: config => {
    config.module
    .rule('svg')
    .test( /\.svg$/)
    .use('vue-svg-loader')
    .loader('vue-svg-loader')
    .end()
  }
}

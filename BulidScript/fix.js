const fs = require('fs-extra');
const ncp = require('ncp').ncp;
ncp.limit = 16;

const content = fs.readFileSync('/media/work/PycharmProjects/td-project/dist/index.html', 'utf8');

const urlsRegex = /(src|href)=([^?!"].*?[^?!"])((?=>)|(?= ))/g;

function replacer(match, p1, p2) {
  return `${p1}="{% static '${p2}' %}"`;
}

const urlFixedString = content.replace(urlsRegex, replacer);

const fixedString = "{% load static %}\n" + urlFixedString;

fs.writeFile(
  "/media/work/PycharmProjects/payannameportal/payanname-back/skm/templates/index.html",
  fixedString,
  err => {
    if (err) {
      return console.log(err);
    }

    console.log("Fixed templates/index.html urls!");
  });

// fs.remove(
//   '/home/sajad/Projects/secadv-node/security-advisory-node/SecurityAdvisoryNode/static',
//   function (err) {
//     if (err) {
//       return console.error(err);
//     }
//     console.log('Deleted /SecurityAdvisoryNode/static');
//   });

ncp(
  '/media/work/PycharmProjects/td-project/dist', '/media/work/PycharmProjects/payannameportal/payanname-back/skm/static',
  function (err) {
    if (err) {
      return console.error(err);
    }
    console.log('Moved Statics!');
    fs.unlink(
      '/media/work/PycharmProjects/payannameportal/payanname-back/skm/static/index.html',
      (err) => {
        if (err) throw err;
        console.log('Deleted skm/static/index.html');
      });
  });
